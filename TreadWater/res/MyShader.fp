// Terrain Shader
// Fragment Shader
// by Daniel Waine
// interpolated from examples by
// Richard S. Wright Jr.
// OpenGL SuperBible
#version 130

//uniforms
uniform vec4      ambientColor;
uniform vec4      diffuseColor;
uniform sampler2D colorMap;

//inputs
in vec2 vVaryingTexCoords;
in vec3	vNormalOut;
smooth in vec4 vVaryingColor;
smooth in vec3 vVaryingLightDir;

//outputs
out vec4 vFragColor;

void main(void)
   { 
   vFragColor = vVaryingColor;
   
   // Add in ambient light
    vFragColor += ambientColor;
    vFragColor.rgb = min(vec3(1.0,1.0,1.0), vFragColor.rgb);
   
   vFragColor *= texture(colorMap, vVaryingTexCoords.st);
   }