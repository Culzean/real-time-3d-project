////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#include "PlayerUI.h"
#include "PlayerCamera.h"

bool* keyStates = new bool[256];

GLfloat vAmbientColor[] = { 0.2f, 0.2f, 0.2f, 0.0f };
GLfloat vDiffuseColor[] = { 0.6f, 0.4f, 0.5f, 20.0f };

PlayerUI::PlayerUI()
{
	m_cSpawn = new Spawn();
	m_Frame.SetOrigin(0.0f,0.0f,-4.0f);
	for(int i  = 0; i< 256; i++)
		keyStates[i] = false;
}

PlayerUI::PlayerUI( char* modelFname, char* texFname, PlayerCamera* cameraRef, FirstTerrain* currentLvlRef )
{
	pCamera = cameraRef;
	p_crtLvl = currentLvlRef;
	TextureManager* texManager;

	m_pSndManager = SoundManager::GetInstance();

	soundID = m_pSndManager->loadSound("res/crunch.wav");

	texManager = TextureManager::GetInstance();
	texture = texManager->loadTexture( texFname );
	m_cSpawn = new Spawn( modelFname,  texFname, texture );
	m_cModelLoader = md2model::getInstance();
	m_Frame.SetOrigin(0.0f,0.0f,-4.0f);
	for(int i  = 0; i< 256; i++)
		keyStates[i] = false;
}

bool PlayerUI::init( int mapCenX, int mapCenZ )
{
	radius = (47.883 * 0.5);
	scale = 0.1;
	radius *= scale;
	fallCount =0;

	m_vLightPos[0] = 2.0f;
	m_vLightPos[1] = 2.0f;
	m_vLightPos[2] = -20.0f;
	m_vLightPos[3] = 20.0f;

	m_Frame.SetForwardVector(forwardV);
	m_Frame.SetOrigin(0.0f, 0.0f, 0.0f);

	m_Pos.SetOrigin(0.0f, 0.0f, 0.0f);
	m_Pos.SetForwardVector(1.0f, 0.0f, 0.0f);

	vVel[0] = 0.0f;		vVel[1] = 0.0f;		vVel[2] = 0.0f;

	updateY = 0;
	posY = 180;
	m_Pos.MoveForward( mapCenX);
	m_Pos.MoveRight(-mapCenZ);
	lvlCenX = mapCenX;
	lvlCenZ = mapCenZ;


	theta = 0.0f;		alpha = 0.0f;
	linear = 0.4f;
	rotation = float(m3dDegToRad(pCamera->getRotAngle()));

	lvlBound = p_crtLvl->getSize();

	returnCount = RETURN_COUNTER;

	crntAnim = 2;
	m_cModelLoader->setCurrentAnim(0);

	r.Randomize();
	
	walkSounds[0] = new ALuint(m_pSndManager->loadSound("res/crunch1.wav"));
	walkSounds[1] = new ALuint(m_pSndManager->loadSound("res/crunch2.wav"));
	walkSounds[2] = new ALuint(m_pSndManager->loadSound("res/crunch3.wav"));


	bounds = true;

	return true;
}

PlayerUI::~PlayerUI()
{
	delete m_cSpawn->GetModel();
	delete m_vPlayerForward;
	delete m_cSpawn;
	delete[] walkSounds;
}
void PlayerUI::KeyDown(unsigned char key, int x, int y)
{
	keyStates[key] = true;	
}
void PlayerUI::KeyUp (unsigned char key, int x, int y) 
{   
	keyStates[key] = false;
} 

void PlayerUI::KeyOperations (void) 
{
	float rotation = float(m3dDegToRad(0.1f));
	float linear = 0.2f;
	crntAnim = 1;
	M3DVector3f dir;



	if(keyStates['w'])
		{
			//sndManager->playSound(soundID);
			//myPos.MoveForward(linear);
			vVel[0] = (m_vPlayerDir[0] * linear);
			vVel[2] = (m_vPlayerDir[2] * -linear);
			
			//apply this in update if moving, increase if moving fast
			theta += linear/0.2;
			//crntAnim = 0;
		}
	if(keyStates['d'])
		{
			vVel[0] = (m_vPlayerSide[0] * -linear);
			vVel[2] = (m_vPlayerSide[2] * linear);
			//myPos.MoveRight(-linear);
			alpha -= linear/0.2;
		}
	if(keyStates['s'])
		{
			vVel[0] = (m_vPlayerDir[0] * -linear);
			vVel[2] = (m_vPlayerDir[2] * linear);
			//myPos.MoveForward(-linear);
			theta -= linear/0.2;
			//crntAnim = 0;
		}
	if(keyStates['a'])
		{
			vVel[0] = (-m_vPlayerSide[0] * -linear);
			vVel[2] = (-m_vPlayerSide[2] * linear);
			//myPos.MoveRight(linear);
			alpha += linear/0.2;
		}
	if(keyStates[' '])
	{
		if(m_bOnGround){
			vVel[0] = (m_vPlayerDir[0]  * 25);
			vVel[2] = (m_vPlayerDir[2] * -25);
			vVel[1] = (80);
			m_bOnGround = false;
		}
	}
}

void PlayerUI::render(	GLShaderManager &shaderManager, GLGeometryTransform &transformPipeline, GLMatrixStack &modelViewMatrix, M3DMatrix44f mCamera )
{
	static double current_time = 0;
	static double last_time = 0;
	float dt;

	last_time = current_time;
	current_time = (double)glutGet (GLUT_ELAPSED_TIME) / 100.0;

	dt = (current_time - last_time);

	GLfloat vColour[] = { 1.0f, 1.0f, 1.0f, 1.0f};
	M3DVector3f		tempFor;
	M3DVector3f		tempUp;


	m3dTransformVector4(m_vLightEyePos, m_vLightPos, mCamera);

	modelViewMatrix.PushMatrix();

	modelViewMatrix.MultMatrix(m_Frame);

	//righted for whatever viewing rotation has been applied using the camera's forward vector
	modelViewMatrix.Rotate(theta, cos(rotation), 0.0f ,sin(rotation));
	//now how is the rotation to be righted and applied along the z axis as viewed - whatever the roll that has been applied?
	modelViewMatrix.Rotate(alpha, sin(rotation) ,0.0f, cos(rotation));
	modelViewMatrix.Scale(scale,scale,scale);

	glBindTexture(GL_TEXTURE_2D, texture);


	//shaderManager.UseStockShader(GLT_SHADER_TEXTURE_MODULATE, transformPipeline.GetModelViewProjectionMatrix(), vColour, 0 );
	shaderManager.UseStockShader(GLT_SHADER_TEXTURE_POINT_LIGHT_DIFF, transformPipeline.GetModelViewMatrix(),
								transformPipeline.GetProjectionMatrix(), m_vLightPos, vDiffuseColor, 0);

	m_cModelLoader->Animate(crntAnim,dt);
	m_cModelLoader->RenderFrameItp(	m_cSpawn->GetModel() );

	modelViewMatrix.PopMatrix();

}

void PlayerUI::update()
{
	rotation = float(m3dDegToRad(pCamera->getRotAngle()));
	M3DVector3f		tempOrigin;
	M3DVector3f		upVector;

	m_Frame.SetForwardVector(pCamera->m_vForward);

	//for moving the player, just x and z please
	m_vPlayerDir[0] = pCamera->m_vForward[0];
	m_vPlayerDir[1] = 0.0f;
	m_vPlayerDir[2] = pCamera->m_vForward[2];
	m3dNormalizeVector3(m_vPlayerDir);

	m3dCrossProduct3(m_vPlayerSide, upV, m_vPlayerDir);
	m3dNormalizeVector3(m_vPlayerSide);

	if(vVel[1] > 0 && checkGradient())
	{
		vVel[0] += vVel[0] * -0.8;
		vVel[2] += vVel[2] * -0.8;
	}
	if(!m_bOnGround)
	{
		vVel[1] *= 0.7;
	}
	if( (abs(vVel[0]) > 0) || (abs(vVel[2]) > 0) )
	{
		//maybe we should play a sound
		if( r.Random(1000) < 9 )
			playWlkSnd();
	}

	posY += vVel[1];

	//apply the x and z velocity
	m_Pos.MoveForward(vVel[0]);
	m_Pos.MoveRight(vVel[2]);

	m_Pos.GetOrigin(tempOrigin);
	//apply new Y value
	m_Pos.GetUpVector(upVector);
	//what up vector does myFrame require to draw??
	m_Frame.SetOrigin(tempOrigin);
	m_Frame.SetUpVector(upVector);

	tempOrigin[1] = posY + (radius * 1.1);
	m_Pos.SetOrigin(tempOrigin);


	posX = m_Pos.GetOriginX();
	posZ = m_Pos.GetOriginZ();

	if(checkBounds()){
		updateHeight();
		vDiffuseColor[0] = 0.6f;
	}else
		{
			vDiffuseColor[0] *= 0.93;
			if(vDiffuseColor[0] < 0)
				vDiffuseColor[0] = 0;
			--returnCount;
		}

	applyFriction();

	if(returnCount < 0)
	{
		returnCount = RETURN_COUNTER;
		resetPos();
	}
	
	updateLightPos();

	////////////////////
	///update the position of the camera
	pCamera->update(m_Pos,posY + radius);

}

void PlayerUI::playWlkSnd()
{
	float rand = r.Random( 90 );

	if(rand > 70)
		m_pSndManager->playSound(*walkSounds[0]);
	else if( rand > 20)
		m_pSndManager->playSound(*walkSounds[1]);
	else
		m_pSndManager->playSound(*walkSounds[2]);
}

bool PlayerUI::checkCollision( Object* pO )
{
	//if abs dist > radius
	//with box on x coord
	//
	//
	//x component of vector between the box and the player
	//box - player is different direction from player - box
	float distX = pO->getPosX() - getPosX();
	if(abs(distX) < (pO->getRad() + radius))
	{
		//check the z axis
		float distZ = pO->getPosZ() - getPosZ();
		//check absolute value against the combined radii, or width and height if this is a rectangle
		//but square makes simpler
		if(abs(distZ) < (pO->getRad() + radius))
		{
			M3DVector3f returnV;
			//just resolve on one axis!
			if( abs(distX) > abs(distZ) )
			{
				returnV[2] = 0;
				//find the correct side of box to resolve on
				//dist contains the correct direction. except the z axis doesn't
				//radius is scaler and we must find out if this is to be added to the vector or removed
				if(distX > 0)
					returnV[0] = ( distX - pO->getRad() - radius );
				else
					returnV[0] = ( distX + pO->getRad() + radius );
			}else if(abs(distX) < abs(distZ))
			{
				//same. but negative Z?? very puzzled by that. but that works, + doesn't
				//possible my axis is upside down??
				returnV[0] = 0;
				if(distZ > 0)
					returnV[2] = ( -distZ + pO->getRad() + radius );
				else
					returnV[2] = ( -distZ - pO->getRad() - radius );
			}else
			{
				returnV[0] = 0;
				returnV[2] = 0;
				returnV[1] = 0;
			}
			returnV[1] = 0.0f;

			//apply to velocity vector
			vVel[0] += returnV[0];
			vVel[2] += returnV[2];
			return true;
		}
	}
	return false;
}

bool PlayerUI::checkBound( int coord )
{
	//check will work for either x or z since this is always a square
	if(coord < 0 || coord > lvlBound)
		return false;
	else
		return true;
}

bool PlayerUI::checkBounds()
{
	if(posX < 0 || posX > lvlBound)
		{
			//cout << "No longer within the level  " << ": " << x << endl;
			bounds = false;
		}
	else if(posZ < 0 || posZ > lvlBound)
		{
			//cout << "No long within the level  " << ": " << z << endl;
			bounds = false;
		}
	else
		bounds = true;
		return bounds;
}

void PlayerUI::updateHeight()
{
	float dist = ( (posY) - p_crtLvl->GetScaledHeightAtPoint(posX,posZ) );
	//cout << "y diff: " << dist << endl;
	if((int)(dist * 10 ) == 0)
		{//stop moving if close enough and set apprpriate y value
			m_bOnGround = true;
			vVel[1] = 0;
			posY = p_crtLvl->GetScaledHeightAtPoint(posX,posZ) + 0.09;
		}else
	{//decide if we are falling or climbing
		float tempY = vVel[1];
		if(dist < 0)
		{//climbing
			m_bOnGround = true;
			vVel[1] = p_crtLvl->GetScaledHeightAtPoint(posX,posZ);
			if(abs(vVel[1]) > MAX_CLIMB_VEL)
				vVel[1] = MAX_CLIMB_VEL;
		}
		if(dist > 0)
		{//falling
			m_bOnGround = false;
			vVel[1] = p_crtLvl->GetScaledHeightAtPoint(posX,posZ);
			if(abs(vVel[1]) > MAX_FALL_VEL)
				vVel[1] = -MAX_FALL_VEL;
		}
		//and set new y value
		if( ( abs(tempY) + abs(vVel[1])) > 1.7 )
			++fallCount;
		else
			--fallCount;

		//cout << "fall count : " << fallCount << endl;

		//assign animation based on fall counter
		if(fallCount < 0)
			{
				fallCount =0;
				crntAnim = MD2_RUN;
			}
		else if(fallCount > 10)
			crntAnim = MD2_ATTACK;

		posY += ( vVel[1]);
	}
	
}

bool PlayerUI::checkGradient()
{
	//a crude method to check if the height value in direction of travel is above the current y value
	int forwardX, forwardZ;
	if(vVel[0] > 0){
		if(checkBound(posX +1))
			forwardX = posX +1;
		else
			forwardX = posX;
	}
	else if(vVel[0] < 0){
		if(checkBound(posX -1))
		forwardX = posX - 1;
		else
			forwardX = posX;
	}
	else
		return false;
	if(vVel[2] > 0){
		if(checkBound(posZ +1))
			forwardZ = posZ + 1;
		else
			forwardZ  = posZ;
	}
	else if(vVel[2] < 0){
		if(checkBound(posZ -1))
		forwardZ = posZ - 1;
		else
			forwardZ = posZ;
	}
	else
		return false;

	if( posY < p_crtLvl->GetScaledHeightAtPoint(forwardX, forwardZ) )
		return true;
	else
		return false;
}

void PlayerUI::applyFriction()
{
		//////////////////////////////////
	//find friction gradient to apply
	if(abs(vVel[0]) > linear * 0.7 || abs(vVel[2]) > linear * 0.7)
	{
		vVel[0] *= 0.7;
		vVel[2] *= 0.7;
	}
	else if(abs(vVel[0]) > linear * 0.3 || abs(vVel[2]) > linear * 0.3)
	{
		crntAnim = MD2_PAIN1;
		if(vVel[0] > 0)
			vVel[0] -= FRICTION_SUB;
		else
			vVel[0] += FRICTION_SUB;
		if(vVel[2] > 0)
			vVel[2] -= FRICTION_SUB;
		else
			vVel[2] += FRICTION_SUB;
	}
	else
	{
		crntAnim = MD2_RUN;
		vVel[0] = 0;
		vVel[2] = 0;
	}
}

void PlayerUI::updateLightPos()
{
	float newY;

	if(pCamera->getCamAngle() > 2)
		newY = -4.0f;
	else
		newY = 4.0f;

	m_vLightPos[0] = m_Pos.GetOriginX() + (pCamera->m_vForward[0] * 3);
	m_vLightPos[1] = m_Pos.GetOriginY() - newY;
	m_vLightPos[2] = m_Pos.GetOriginZ() + 2.0f;
	m_vLightPos[3] = 20.0f;
}

void PlayerUI::resetPos()
{
	//find vector between current position and centre of map
	//half the scale of this value
	//render player frame by this x and z value
	//reset y
	cout << "going back" << endl;
	float returnX = (lvlCenX - m_Pos.GetOriginX()) * 0.5;
	float returnZ = (lvlCenZ - m_Pos.GetOriginZ()) * 0.5;

	posY = 180;
	m_Pos.MoveForward( returnX);
	m_Pos.MoveRight(-returnZ);
}

