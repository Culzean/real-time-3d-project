////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#include "FirstTerrain.h"

GLfloat vColour[] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat vColour2[] = {1.0f,0.0f,0.0f,1.0f};
//will need to load colours using height map at some point

FirstTerrain::~FirstTerrain()
{
	for(int i=0; i<m_iTotBlks; i++)
		delete terrainBlocks[i];
	FirstTerrain::UnloadHeightMap();
}

void FirstTerrain::flattenSquare( int centreX, int centreZ, int radius, float desScaleHeight )
{
	int startX = centreX - radius;
	int startZ = centreZ - radius;
	int endX = centreX + radius;
	int endZ = centreZ + radius;
	float newHeight = desScaleHeight / GetHeightScale();

	for( int i= startX; i < endX ; ++i )
	{
		if(i > 0 && i < m_iSize)
		{

			for( int j = startZ; j< endZ; ++j )
			{
				//check each x,z for being over the boundary.
				//but continue with the opperation after the value has been thrown
				if(j > 0 && j < m_iSize)
				{
					SetHeightAtPoint( newHeight, i, j );
				}
			}
		}
	}
}

void FirstTerrain::Setup()
{
	TextureManager* myTexLoad = TextureManager::GetInstance();

	myTex = myTexLoad->getTexID("res/detailMap.tga");


	m_iNoBlkSide = m_iSize / NO_VERTS_BLOCK;
	m_iTotBlks = m_iNoBlkSide * m_iNoBlkSide;

	if(m_iSize % NO_VERTS_BLOCK != 0)
		{
			cout << "This map size is not a factor of the basic terrain block. Cannot continue" << endl;
			return;
		}

	if( m_iTotBlks > MAX_NO_TERRAIN_BLOCKS )
		{
			cout << "This map size is too large. Cannot continue" << endl;
			return;
		}

	SetPlayerStart();

	for(int i=0; i< m_iTotBlks; i++)
	{
		terrainBlocks[i] = new TerrainBatch();
		blockFrames[i] = floorFrame;
		
		int iZ = (i / m_iNoBlkSide);
		int iX = (i % m_iNoBlkSide);
		iZ *= NO_VERTS_BLOCK;
		iX *= NO_VERTS_BLOCK;


		if(!generateTerrainPatch( terrainBlocks[i], iX, iZ ))
			{
				cout << "There is trouble generating terrain" <<  endl;
				m_bBuilt = false;
			}
			

		blockFrames[i].MoveForward(-iZ);
		blockFrames[i].MoveRight(-iX);
	}

	//we have built the terrain
	m_bBuilt = true;

	if(!generateShader())
		cout << "There is trouble creating the shaders" << endl;
}

void FirstTerrain::SetPlayerStart()
{
	//use a set location for the meantime.
	//preferably search for a location on the height map that is neither too high or too low.
	//Then start the player far above
	//but.. just a above with no gravity

	floorFrame.SetOrigin(0.0f, 0.0f , 0.0f );
	floorFrame.SetForwardVector(0.0f,0.0f,-1.0f);

	floorFrame2 = floorFrame;
	floorFrame2.MoveRight(m_iSize);
}
	

void FirstTerrain::Render(GLShaderManager &shaderManager, GLMatrixStack &modelViewMatrix, GLGeometryTransform &transformPipeline )
{
	GLfloat	rot m3dDegToRad(-90);
	//glEnable(GL_TEXTURE_2D);
	
	GLfloat vEyeLight[] = { -100.0f, 100.0f, 100.0f };;//how difficult are these to change?
	GLfloat vAmbientColor[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	GLfloat vDiffuseColor[] = { 0.5f, 0.4f, 0.5f, 1.0f};

	for(int i=0; i< m_iTotBlks; i++)
	{
	modelViewMatrix.PushMatrix();

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glCullFace(GL_BACK);


		modelViewMatrix.MultMatrix(blockFrames[i]);

		modelViewMatrix.Rotate(180,0.0f,1.0f,0.0f);

		glUseProgram(myIdentityShader);
		glUniform4fv(myLocAmbient, 1, vAmbientColor);
		glUniform4fv(myLocDiffuse, 1, vDiffuseColor);
		glUniform3fv(myLocLight, 1, vEyeLight);
		glUniformMatrix4fv(myLocMVP, 1, GL_FALSE, transformPipeline.GetModelViewProjectionMatrix());
		glUniformMatrix4fv(myLocMV, 1, GL_FALSE, transformPipeline.GetModelViewMatrix());
	//I had not idea you could do this
		glUniformMatrix3fv(myLocNM, 1, GL_FALSE, transformPipeline.GetNormalMatrix());
	
		GLint iTextureUniform = glGetUniformLocation(myIdentityShader, "colorMap");

		glUniform1i(iTextureUniform, 0);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, myTex);
		
		terrainBlocks[i]->Draw();
	modelViewMatrix.PopMatrix();

	}

	glCullFace(GL_FRONT);

}

bool FirstTerrain::generateShader()
{
		//basic shader program. Loaded with 3 !, attributes, 0 and 1
	myIdentityShader = gltLoadShaderPairWithAttributes("res/MyShader.vp", "res/MyShader.fp", 3,
														GLT_ATTRIBUTE_VERTEX, "vVertex",
														GLT_ATTRIBUTE_TEXTURE0, "vTexCoords",
														GLT_ATTRIBUTE_NORMAL, "vNorms");

	//glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);


/*	GLint	myIdentityShader;
	GLint	myLocMVP;
	GLint	myLocMV;
	GLint	myLocNM;		//normal matrix uniform
	GLuint	myTex;
	GLint	myLocColourMap;		//do i need a colour map uniform?
	GLint	myLocLight;*/

	///load up on uniforms
	myLocMVP = glGetUniformLocation(myIdentityShader, "mvpMatrix");
	myLocMV = glGetUniformLocation(myIdentityShader, "mvMatrix");
	myLocNM = glGetUniformLocation(myIdentityShader, "normalMatrix");

	myLocLight = glGetUniformLocation(myIdentityShader, "vLightPosition");
	myLocAmbient = glGetUniformLocation(myIdentityShader, "ambientColor");
	myLocDiffuse = glGetUniformLocation(myIdentityShader, "diffuseColor");

	return true;
}

bool FirstTerrain::generateTerrainPatch( TerrainBatch *newBatch, int mapX, int mapZ )
{
	newBatch->BeginMesh(	NO_VERTS_BLOCK * NO_VERTS_BLOCK * 10	);

	for(int iZ =0; iZ< NO_VERTS_BLOCK; iZ++ )
	{
		float coordZ1 = (float)mapZ/m_iSize;
		float coordZ2 = (float)(mapZ+1)/m_iSize;
		//end up using double the number of vertices than I should be on the Z axis
	for(int iX = 0; iX < NO_VERTS_BLOCK; iX++)
		{
			float coordX1 = (float)mapX/m_iSize;
			float coordX2 = (float)(mapX + 1)/m_iSize;
			M3DVector3f verts[4];
			M3DVector3f vNorms[4];
			M3DVector2f vTexCoords[4];

			verts[0][0] = GLfloat(iX);
			verts[0][1] = GetScaledHeightAtPoint(mapX,mapZ);
			verts[0][2] = GLfloat(iZ);

			verts[1][0] = GLfloat(iX);
			verts[1][1] = GetScaledHeightAtPoint(mapX,mapZ+1);
			verts[1][2] = GLfloat(iZ+1);

			verts[2][0] = GLfloat(iX+1);
			verts[2][1] = GetScaledHeightAtPoint(mapX+1,mapZ);
			verts[2][2] = GLfloat(iZ);

			verts[3][0] = GLfloat(iX+1);
			verts[3][1] = GetScaledHeightAtPoint(mapX+1,mapZ+1);
			verts[3][2] = GLfloat(iZ+1);

			///not certain I'm getting the correct normals here
			newBatch->FindSurfaceNormal(vNorms[0], verts[0], verts[1], verts[2] );
			newBatch->FindSurfaceNormal(vNorms[1], verts[1], verts[2], verts[0] );
			newBatch->FindSurfaceNormal(vNorms[2], verts[2], verts[0], verts[1] );
			newBatch->FindSurfaceNormal(vNorms[3], verts[3], verts[2], verts[1] );

			
			//m3dScaleVector3(vNorms, -1.0f);

			vTexCoords[0][0] = coordX1;
			vTexCoords[0][1] = coordZ1;

			vTexCoords[1][0] = coordX1;
			vTexCoords[1][1] = coordZ2;

			vTexCoords[2][0] = coordX2;
			vTexCoords[2][1] = coordZ1;

			vTexCoords[3][0] = coordX2;
			vTexCoords[3][1] = coordZ2;

			newBatch->AddTriangle( verts, vNorms, vTexCoords );

			//second triangle

			memcpy(verts[0], verts[1], sizeof(M3DVector3f));
			memcpy(vNorms[0], vNorms[1], sizeof(M3DVector3f));
			memcpy(vTexCoords[0], vTexCoords[1], sizeof(M3DVector2f));

			//perform swap
			//temp placement of right lower vertex into last spot
			memcpy(verts[1], verts[3], sizeof(M3DVector3f));
			memcpy(vNorms[1], vNorms[3], sizeof(M3DVector3f));
			memcpy(vTexCoords[1], vTexCoords[3], sizeof(M3DVector2f));

			newBatch->AddTriangle( verts, vNorms, vTexCoords );

			mapX++;
		}
	//house keeping with the texture and heightmap coordinates
	//these are not quite part of the nested for loops here
		mapX -= NO_VERTS_BLOCK;
		mapZ++;
	}
	newBatch->End();
	return true;
}

float FirstTerrain::getHeightInversed(int x, int z)
{
	rightX = x;
	rightZ = z;
	rightY = GetScaledHeightAtPoint(x,z);

	return rightY;
}


void FirstTerrain::TidyUp()
{
	glDeleteProgram(myIdentityShader);
}
