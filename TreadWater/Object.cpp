////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#include "Object.h"


Object::Object( char* modelFname, char* texFname )
{
	TextureManager* texManager;

	texManager = TextureManager::GetInstance();
	m_iTexture = texManager->loadTexture( texFname );

	m_cSpawn = new Spawn( modelFname, texFname, m_iTexture );
	m_cModelLoader = md2model::getInstance();
	m_icrntAnim = 0;
}

Object::~Object()
{
	delete m_cSpawn;
}

bool Object::init( int startX, int startY ,int startZ, float iSize )
{
	float rot = m3dDegToRad(180);
	m_fRadius = iSize;
	m_iGoalCount = 0;
	m_bGoalTrig = false;
	m_frPos.SetForwardVector(forwardV);
	m_frPos.SetUpVector(upV);
	m_frPos.MoveForward(-startZ);
	m_frPos.MoveRight(-startX);
	m_frPos.MoveUp(startY);

	//m_frPos.RotateLocalX(rot);

	return true;
}

bool Object::update()
{
	if(m_iGoalCount > GOAL_TRIG )
	{
		//yOU WIN this time!
		m_bGoalTrig = true;
		m_iGoalCount = 0;
		return true;
	}
	return false;
}

bool Object::checkPlayer( PlayerUI* pPlayer, int testRad )
{
	float distX = getPosX() - pPlayer->getPosX();
	if(abs(distX) < (testRad))
	{
		//check the y axis
		float distZ = getPosZ() - pPlayer->getPosZ();
		if(abs(distZ) < (testRad))
		{
			return true;
		}
	}
	return false;
}

void Object::render( GLShaderManager &shaderManager, GLGeometryTransform &transformPipeline, GLMatrixStack &modelViewMatrix, M3DMatrix44f mCamera )
{
	GLfloat vColour[] = { 1.0f, 1.0f, 1.0f, 1.0f};
	//with lighting
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	//glCullFace(GL_FRONT);
	m3dTransformVector4(m_vLightEyePos, m_vLightPos, mCamera);

	modelViewMatrix.PushMatrix();

	modelViewMatrix.Translate(0.0f,-m_fRadius,-m_fRadius*2);

	modelViewMatrix.MultMatrix(m_frPos);

	glBindTexture(GL_TEXTURE_2D, m_iTexture);

	modelViewMatrix.Rotate(-90, 1.0f, 0.0f, 0.0f);
	modelViewMatrix.Scale(0.01,0.01,0.01);
	//modelViewMatrix.Translate(-m_fRadius,0.0f,m_fRadius);
	//modelViewMatrix.Rotate(180, 0.0f, 0.0f, 1.0f);
	shaderManager.UseStockShader(GLT_SHADER_TEXTURE_MODULATE, transformPipeline.GetModelViewProjectionMatrix(), vColour, 0);

	//m_cModelLoader->Animate(0,1);
	m_cModelLoader->RenderFrameItp(	m_cSpawn->GetModel() );

	modelViewMatrix.PopMatrix();

	M3DVector3f corner1, corner2;
	boxP1[0] = m_frPos.GetOriginX() - m_fRadius;			boxP2[0] = m_frPos.GetOriginX() + m_fRadius;
	boxP1[1] = m_frPos.GetOriginY() - m_fRadius;			boxP2[1] = m_frPos.GetOriginY() + m_fRadius;
	boxP1[2] = m_frPos.GetOriginZ() - m_fRadius;			boxP2[2] = m_frPos.GetOriginZ() + m_fRadius;
}

void Object::render( GLShaderManager &shaderManager, GLGeometryTransform &transformPipeline, GLMatrixStack &modelViewMatrix )
{//no lighting
	modelViewMatrix.PushMatrix();

	modelViewMatrix.MultMatrix(m_frPos);

	glBindTexture(GL_TEXTURE_2D, m_iTexture);

	shaderManager.UseStockShader(GLT_SHADER_TEXTURE_MODULATE, transformPipeline.GetModelViewProjectionMatrix(), m_vLightEyePos, 0);

	m_cModelLoader->RenderFrameItp(	m_cSpawn->GetModel() );

	modelViewMatrix.PopMatrix();
}