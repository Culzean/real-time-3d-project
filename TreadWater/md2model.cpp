////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

/* md2model.cpp - based on original:
 * md2.c -- md2 model loader
 * last modification: November 2010 by Daniel Livingstone
 *
 * Copyright (c) 2005-2007 David HENRY
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * gcc -Wall -ansi -lGL -lGLU -lglut md2.c -o md2
 */

#define WIN32
#include <GLTools.h>
#define FREEGLUT_STATIC
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md2model.h"

	//static std::map<std::string, md2_model_t*>	modelMap;
	//std::map<std::string, md2_model_t*>::iterator modelIndex;

	md2model* md2model::pModelLoader = 0;

/* Table of precalculated normals */
M3DVector3f anorms_table[162] = {
#include "anorms.h"
};

// Table of animation begin/end frames
static int animFrameList[40] = {
	0, 39, // stand//forwardroll
	40, 45, // run//stand
	46, 53, // attack
	54, 57, //pain1
	58, 61, //pain2
	62, 65, //pain3
	66, 71, //jump
	72, 83, //flip//sideroll+
	84, 94, //salute//sideroll-
	95, 111, //taunt
	112, 122, //wave
	123, 134, //point
	135, 153, //crstnd
	154, 159, //crwalk
	160, 168, //crattak
	169, 172, //crpain
	173, 177, //crdeath
	178, 183, //death1
	184, 189, //death2
	190, 196 //death3
};

md2model::md2model()
{
	currentAnim = 0;
	currentFrame = 0;
	nextFrame = 1;
	interp = 0.0f;
}

md2model::md2model(const char *filename)
{
	ReadMD2Model(filename);
	triangles = new GLBatch();
	currentAnim = 0;
	currentFrame = 0;
	nextFrame = 1;
	interp = 0.0f;
}

md2model::~md2model()
{
	clearModels();
}

void md2model::clearModels()
{
	for(modelIndex = modelMap.begin(); modelIndex != modelMap.end(); modelIndex++)
		FreeModel(modelIndex->second);
}

md2_model_t* md2model::GetMD2Model(const char *filename)
{
	//check static map for a point to this filename.
	//if we have that pointer
	//returns
	//if not
	//attempt to load this model into map
	//return loaded pointer

	modelIndex = modelMap.find(filename);

	if(modelIndex == modelMap.end())
	{//the model is not loaded into this map
		md2_model_t* modelRef = ReadMD2Model(filename);
		if(modelRef)
			modelMap.insert( std::pair< std::string, md2_model_t* >(filename, modelRef) );
		else
			{
				std::cout << "Problem loading this file. Please check filename!" << std::endl;
				return NULL;
			}
	}
	for( modelIndex = modelMap.begin(); modelIndex != modelMap.end(); modelIndex++ )
		cout << "keys: " << modelIndex->first << "  and " << modelIndex->second << endl;


	//this wouldn't work if modelMap was a pointer.
	//very annoying
	return modelMap[filename];
}

/**
 * Load an MD2 model from file.
 *
 * Note: MD2 format stores model's data in little-endian ordering.  On
 * big-endian machines, you'll have to perform proper conversions.
 */
md2_model_t* md2model::ReadMD2Model (const char *filename)
{
  FILE *fp;
  int i;

  fp = fopen (filename, "rb");
  if (!fp)
    {
      fprintf (stderr, "Error: couldn't open \"%s\"!\n", filename);
      return 0;
    }

  md2_model_t* new_mdl = new md2_model_t();

  /* Read header */
  fread (&new_mdl->header, 1, sizeof (struct md2_header_t), fp);

  if ((new_mdl->header.ident != 844121161) ||
      (new_mdl->header.version != 8))
    {
      /* Error! */
      fprintf (stderr, "Error: bad version or identifier\n");
      fclose (fp);
      return 0;
    }

  /* Memory allocations */
  new_mdl->skins = (struct md2_skin_t *)
    malloc (sizeof (struct md2_skin_t) * new_mdl->header.num_skins);
  new_mdl->texcoords = (struct md2_texCoord_t *)
    malloc (sizeof (struct md2_texCoord_t) * new_mdl->header.num_st);
  new_mdl->triangles = (struct md2_triangle_t *)
    malloc (sizeof (struct md2_triangle_t) * new_mdl->header.num_tris);
  new_mdl->frames = (struct md2_frame_t *)
    malloc (sizeof (struct md2_frame_t) * new_mdl->header.num_frames);
  new_mdl->glcmds = (int *)malloc (sizeof (int) * new_mdl->header.num_glcmds);

  /* Read model data */
  fseek (fp, new_mdl->header.offset_skins, SEEK_SET);
  fread (new_mdl->skins, sizeof (struct md2_skin_t),
	 new_mdl->header.num_skins, fp);

  fseek (fp, new_mdl->header.offset_st, SEEK_SET);
  fread (new_mdl->texcoords, sizeof (struct md2_texCoord_t),
	 new_mdl->header.num_st, fp);

  fseek (fp, new_mdl->header.offset_tris, SEEK_SET);
  fread (new_mdl->triangles, sizeof (struct md2_triangle_t),
	 new_mdl->header.num_tris, fp);

  fseek (fp, new_mdl->header.offset_glcmds, SEEK_SET);
  fread (new_mdl->glcmds, sizeof (int), new_mdl->header.num_glcmds, fp);

  /* Read frames */
  fseek (fp, new_mdl->header.offset_frames, SEEK_SET);
  for (i = 0; i < new_mdl->header.num_frames; ++i)
    {
      /* Memory allocation for vertices of this frame */
      new_mdl->frames[i].verts = (struct md2_vertex_t *)
	malloc (sizeof (struct md2_vertex_t) * new_mdl->header.num_vertices);

      /* Read frame data */
      fread (new_mdl->frames[i].scale, sizeof (M3DVector3f), 1, fp);
      fread (new_mdl->frames[i].translate, sizeof (M3DVector3f), 1, fp);
      fread (new_mdl->frames[i].name, sizeof (char), 16, fp);
      fread (new_mdl->frames[i].verts, sizeof (struct md2_vertex_t),
	     new_mdl->header.num_vertices, fp);
    }

  fclose (fp);
  //triangles.Begin(GL_TRIANGLES,(GLuint)new_mdl->header.num_tris * 3,1);

  return new_mdl;
}


/**
 * Free resources allocated for the model.
 */
void md2model::FreeModel (md2_model_t* old_model)
{
  int i;

  if (old_model->skins)
    {
      free (old_model->skins);
      old_model->skins = NULL;
    }


  if (old_model->texcoords)
    {
      free (old_model->texcoords);
      old_model->texcoords = NULL;
    }

  if (old_model->triangles)
    {
      free (old_model->triangles);
      old_model->triangles = NULL;
    }

  if (old_model->glcmds)
    {
      free (old_model->glcmds);
      old_model->glcmds = NULL;
    }

  if (old_model->frames)
    {
      for (i = 0; i < old_model->header.num_frames; ++i)
	{
	  free (old_model->frames[i].verts);
	  old_model->frames[i].verts = NULL;
	}

      free (old_model->frames);
      old_model->frames = NULL;
    }
}


/**
 * Render the model with interpolation between frame n and n+1.
 * interp is the interpolation percent. (from 0.0 to 1.0)
 */
void md2model::RenderFrameItp(md2_model_t* mdl)
{
  int i, j;
  GLfloat s, t;
  M3DVector3f v, norm, v_curr, v_next;
  float *n_curr, *n_next;
  struct md2_frame_t *pframe1, *pframe2;
  struct md2_vertex_t *pvert1, *pvert2;

  /* Check if currentFrame is in a valid range */
  if ((currentFrame < 0) || (currentFrame > mdl->header.num_frames))
    return;
  
  // Clear the current batch contents
  // This previously used the GLBatch Reset method - but this does not
  // release the array memory - resulting in a massive memory leak.
  // Instead, have to delete and re-new the GLBatch...
  // Alternatively, could fix the GLBatch code or create a GLDynamicBatch class
  //triangles.Reset();
//  if(triangles != NULL)
//	delete triangles;
  triangles = new GLBatch;
  /* Add model to batch */
  triangles->Begin(GL_TRIANGLES,(GLuint)mdl->header.num_tris * 3,1);
	  
    /* Draw each triangle */
    for (i = 0; i < mdl->header.num_tris; ++i)
      {
	/* Draw each vertex */
	for (j = 0; j < 3; ++j)
	  {
	    pframe1 = &mdl->frames[currentFrame];
	    pframe2 = &mdl->frames[nextFrame];
	    pvert1 = &pframe1->verts[mdl->triangles[i].vertex[j]];
	    pvert2 = &pframe2->verts[mdl->triangles[i].vertex[j]];

	    /* Compute texture coordinates */
	    s = (GLfloat)mdl->texcoords[mdl->triangles[i].st[j]].s / mdl->header.skinwidth;
	    t = (GLfloat)mdl->texcoords[mdl->triangles[i].st[j]].t / mdl->header.skinheight;

	    /* Pass texture coordinates to batch */
		triangles->MultiTexCoord2f(0,s,t);

		/* Interpolate normals */
	    n_curr = anorms_table[pvert1->normalIndex];
	    n_next = anorms_table[pvert2->normalIndex];

	    norm[0] = n_curr[0] + interp * (n_next[0] - n_curr[0]);
	    norm[1] = n_curr[1] + interp * (n_next[1] - n_curr[1]);
	    norm[2] = n_curr[2] + interp * (n_next[2] - n_curr[2]);

		triangles->Normal3fv(norm);

	    /* Interpolate vertices */
		// Doing these scaling operations *every* refresh is *very* wasteful
		// Should do all the scaling calculations once only, when loading the file
	    v_curr[0] = pframe1->scale[0] * pvert1->v[0] + pframe1->translate[0];
	    v_curr[1] = pframe1->scale[1] * pvert1->v[1] + pframe1->translate[1];
	    v_curr[2] = pframe1->scale[2] * pvert1->v[2] + pframe1->translate[2];

	    v_next[0] = pframe2->scale[0] * pvert2->v[0] + pframe2->translate[0];
	    v_next[1] = pframe2->scale[1] * pvert2->v[1] + pframe2->translate[1];
	    v_next[2] = pframe2->scale[2] * pvert2->v[2] + pframe2->translate[2];

	    v[0] = v_curr[0] + interp * (v_next[0] - v_curr[0]);
	    v[1] = v_curr[1] + interp * (v_next[1] - v_curr[1]);
	    v[2] = v_curr[2] + interp * (v_next[2] - v_curr[2]);

		triangles->Vertex3fv(v);
	  }
    }
	triangles->End();
	triangles->Draw();
	delete triangles;
}


/**
 * Calculate the current frame in animation, based on
 * selected animation and current frame. If current frame
 * is not in selected animation sequence, new current frame
 * is start of animation sequence. 
 * If interpolation is past 1.0, move current frame to next
 * and next frame = current frame + 1
 */
void md2model::Animate (int animation, float dt)
{
	int start = animFrameList[animation * 2];
	int end =  animFrameList[animation * 2 + 1];
	if ((currentFrame < start) || (currentFrame > end))
	{
		currentFrame = start;
		nextFrame = start + 1;
	}
	interp += dt;
	if (interp >= 1.0f)
    {

		/* Move to next frame */
		interp = 0.0f;
		currentFrame = nextFrame;
		nextFrame++;

		if (nextFrame >= end+1)
		nextFrame = start;
    }
}

