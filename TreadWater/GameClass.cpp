////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#include "game.h"
#include "PlayerCamera.h"

GameClass* GameClass::pGame = 0;

md2model*			loader;

bool* oldKeys = new bool[256];


char scrPrint [50];

GLFrame		torusFrame;
Object*		testObject;


void GameClass::SetupWindow(int argc, char* argv[])
{
	gltSetWorkingDirectory(argv[0]);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH );
	glutInitWindowSize(1024, 800);
	glutCreateWindow("TreadWater");

	GLenum err = glewInit();
	if(GLEW_OK != err)
	{
		//we have a problem creating this window
		std::cerr << "Error: " <<
			glewGetErrorString(err) << std::endl;
		glutLeaveMainLoop();
	}
	//setup a few extra conditions outside the main loop
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE,
				GLUT_ACTION_CONTINUE_EXECUTION);
}

void GameClass::Update()
{
	//	//main call to current level class render
	myPlayer->KeyOperations();

	myPlayer->checkCollision(testObject);

	myObjectives->Update();

	myObjectives->checkPlayer(myPlayer);

	myPlayer->update();
}

bool GameClass::Init( int iSize, int iHilly )
{
	Reshape(1024,800);
	int mapSize;
	float filter;
	int smallStep, largeStep;


	switch(iSize){
	case 0:
		mapSize = 64;
		break;
	case 1:
		mapSize = 96;
		break;
	case 2:
		mapSize = 128;
		break;
	default:
		mapSize = 128;
		break;
	};

	switch(iHilly){
	case 0:
		filter = 0.7;
		smallStep = 4;
		largeStep = 9;
		break;
	case 1:
		filter = 0.3;
		smallStep = 8;
		largeStep = 11;
		break;
	case 2:
		filter = 0.2;
		smallStep = 9;
		largeStep = 12;
		break;
	default:
		filter = 0.3;
		smallStep = 8;
		largeStep = 11;
		break;
	};

	loader->getInstance();

	crtLvlSize = 128;
	dt = 0.04;
	ticks = clock();
	beginTime = ticks;
	nextFrame = ticks + dt * CLOCKS_PER_SEC;		//no rendering change of time yet!
	frames = 0;

	mouseX = 0;		mouseY = 0;

	//create object to display
	glClearColor(1.0, 1.0, 1.0, 0.0); // set background colour
	glColor3f(0.0, 1.0, 0.0); // set drawing colour
	gluOrtho2D(0,1020,0,800); 

	for(int i =0; i < 256; i++)
		oldKeys[i] = false;
	
	freeLook = false;

	// Initialze Shader Manager
	shaderManager.InitializeStockShaders();
	
	glEnable(GL_DEPTH_TEST);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
    
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	// This makes a torus
	gltMakeTorus(torusBatch, 0.4f, 0.15f, 30, 30);
	
	//make my first terrain
	myWorld = new FirstTerrain();
    mySky = new SkyBox();

	//myWorld->LoadHeightMap("height128.raw", 128);
	//maybe want to ask the player to set some of these variables
	myWorld->MakeTerrainFault(mapSize, 16, smallStep, largeStep, filter);
	myWorld->SetHeightScale(0.65f);
	//call to generate a new height map. size of square map in units. no of times algorithm is run. minHeight change, maxHeightchange, filter strength
	

	//create map and world. But init objectives before we create the mesh for the world
	myObjectives = new Objectives();
	myObjectives->Init(myWorld);
	
	//create mesh and shader
	myWorld->Setup();

	myCamera = new PlayerCamera(myPlayer);
	myPlayer = new PlayerUI( "res/player.md2", "res/BeachBallSmall.tga", myCamera, myWorld );
	myPlayer->init( myWorld->getSize() * 0.5, myWorld->getSize() * 0.5 );

	mySky->Init();

	testObject = new Object( "res/WaterDisp.md2", "res/waterdisp.tga" );
	testObject->init(15,myWorld->GetScaledHeightAtPoint(15,15), 15,2.6 );
	//myPlayer->init();

	pause = false;

	return true;

}

void GameClass::RenderDebug()
{
	float x,y,z,rad;

	x = myPlayer->getPosX();
	y = myPlayer->getPosY();
	z = myPlayer->getPosZ();
	rad = myPlayer->getRadius();
	M3DVector3f fV,uV, floorO;
	myPlayer->GetFrame().GetForwardVector(fV);
	myPlayer->GetFrame().GetUpVector(uV);
	myWorld->getO().GetOrigin(floorO);

	glDisable(GL_DEPTH_BUFFER);
	glPointSize(10);
	glColor3f(1.0f,0.0f,0.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0f,myWorld->GetScaledHeightAtPoint(0,0),0.0f);
    glVertex3f (myPlayer->getPosX(), myPlayer->getPosY(), myPlayer->getPosZ());
	//top x,z
	glEnd();
	glBegin(GL_LINES);
	glVertex3f (x - rad , y + rad, z - rad);
	glVertex3f (x + rad , y + rad, z - rad);
	glVertex3f (x + rad , y + rad, z + rad);
	glVertex3f (x - rad , y + rad, z + rad);
	glEnd();
	//rightside z,y
	/*glBegin(GL_LINES);
	glVertex3f (x + rad , y + rad, z - rad);
	glVertex3f (x + rad , y + rad, z + rad);
	glVertex3f (x + rad , y + rad, z + rad);
	glVertex3f (x + rad , y + rad, z - rad);
	glEnd();
	//leftside z,y
	glBegin(GL_LINES);
	glVertex3f (x - rad , y - rad, z - rad);
	glVertex3f (x - rad , y - rad, z + rad);
	glVertex3f (x - rad , y + rad, z + rad);
	glVertex3f (x - rad , y + rad, z - rad);
	glEnd();

	//bottom x,z
	glBegin(GL_LINES);
	glVertex3f (x - rad , y - rad, z - rad);
	glVertex3f (x + rad , y - rad, z - rad);
	glVertex3f (x + rad , y - rad, z + rad);
	glVertex3f (x - rad , y - rad, z + rad);
	glEnd();*/

	glBegin(GL_LINES);
	glVertex3f (x , y, z);
	glVertex3f (uV[0]*10,uV[1]*10,uV[2]*10);
	/*glEnd();

	glBegin(GL_LINES);
	glVertex3f (x , y, z);*/
	glVertex3f (fV[0]*10,fV[1]*10,fV[2]*10);
	glEnd();
	GLfloat vColour[] = { 0.4f, 0.0f, 0.0f, 1.0f };
	shaderManager.UseStockShader(GLT_SHADER_FLAT, transformPipeline.GetModelViewProjectionMatrix(), vColour);

	glBegin(GL_LINES);
	glColor3f(0.0f,1.0f,0.0f);
	glVertex3f (floorO[0] , floorO[1], floorO[2]);
	glVertex3f (myWorld->rightX,myWorld->rightY,myWorld->rightZ);
	glEnd();

	glEnable(GL_DEPTH_BUFFER);
}

void GameClass::drawColBox( M3DVector3f p1, M3DVector3f p2 )
{
	GLfloat vColour[] = { 0.4f, 0.0f, 0.0f, 1.0f };
	shaderManager.UseStockShader(GLT_SHADER_FLAT, transformPipeline.GetModelViewProjectionMatrix(), vColour);


                glPushAttrib(GL_ENABLE_BIT);
                    glDisable(GL_LIGHTING);
					glDisable(GL_DEPTH);
                    glLineWidth(5);
                    glPointSize(10);
                    glBegin(GL_LINES);
                        glVertex3fv(p1);
                        glVertex3f(p2[0], p1[1], p1[2]);

                        glVertex3f(p2[0], p1[1], p1[2]);
                        glVertex3f(p2[0], p2[1], p1[2]);

                        glVertex3f(p2[0], p2[1], p1[2]);
                        glVertex3f(p1[0], p2[1], p1[2]);

                        glVertex3f(p1[0], p2[1], p1[2]);
                        glVertex3f(p1[0], p1[1], p1[2]);

                        //The same for the upper rectangle
                        glVertex3fv(p2);
                        glVertex3f(p1[0], p2[1], p2[2]);

                        glVertex3f(p1[0], p2[1], p2[2]);
                        glVertex3f(p1[0], p1[1], p2[2]);

                        glVertex3f(p1[0], p1[1], p2[2]);
                        glVertex3f(p2[0], p1[1], p2[2]);

                        glVertex3f(p2[0], p1[1], p2[2]);
                        glVertex3f(p2[0], p2[1], p2[2]);

                        //Side lines
                        glVertex3fv(p2);
                        glVertex3f(p2[0], p2[1], p1[2]);

                        glVertex3fv(p1);
                        glVertex3f(p1[0], p1[1], p2[2]);

                        glVertex3f(p2[0], p1[1], p2[2]);
                        glVertex3f(p2[0], p1[1], p1[2]);

                        glVertex3f(p2[0], p1[1], p2[2]);
                        glVertex3f(p2[0], p1[1], p1[2]);

                        glVertex3f(p1[0], p2[1], p2[2]);
                        glVertex3f(p1[0], p2[1], p1[2]);
                    glEnd();
                    glBegin(GL_POINTS);
                        glVertex3fv(p1);
                        glVertex3fv(p2);
                    glEnd();
                glPopAttrib();
}

void GameClass::Render()
{
	static GLfloat vColour[] = { 0.4f, 1.0f, 0.8f, 1.0f };
	M3DVector3f	corner1, corner2;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	modelViewMatrix.PushMatrix();//identity matrix loaded

	M3DMatrix44f	mCamera;

	if(	!myObjectives->getComplete() )
		myCamera->getFrCam().GetCameraMatrix(mCamera);
	else
		myCamera->getHighFrame( crtLvlSize ).GetCameraMatrix(mCamera);

	if(	!myObjectives->getComplete() )
		mySky->Render(myCamera->getFrCam(), modelViewMatrix, transformPipeline);
	

	modelViewMatrix.PushMatrix(mCamera);
	modelViewMatrix.PushMatrix();
	
		myPlayer->render( shaderManager, transformPipeline, modelViewMatrix, mCamera );

		myWorld->Render(shaderManager,modelViewMatrix, transformPipeline );

		myObjectives->Render(shaderManager, transformPipeline, modelViewMatrix, mCamera);
		
		testObject->render(shaderManager, transformPipeline, modelViewMatrix, mCamera);

		for(int i= 0; i < 3; i++)
		{
			myObjectives->getObject(i)->getBoxP1(corner1);
			myObjectives->getObject(i)->getBoxP2(corner2);

			drawColBox(corner1, corner2);
		}

		shaderManager.UseStockShader(GLT_SHADER_FLAT, transformPipeline.GetModelViewProjectionMatrix(), vColour );

		torusBatch.Draw();

	modelViewMatrix.PopMatrix();

	

		shaderManager.UseStockShader(GLT_SHADER_FLAT, transformPipeline.GetModelViewProjectionMatrix(), vColour);
		RenderDebug();

	modelViewMatrix.PopMatrix();

	
	modelViewMatrix.PopMatrix();

	//display flat


	// Do the buffer Swap
	glutSwapBuffers();
}

void GameClass::Display(void)
{
	//Display is called as often as glut can be bothered
	//so make check here for time passed

	if(!pause)
	{
	//must also check if pause has been activated
	//if true must continue display buffers swaps but no update call
	//will need to record the clock() time when this is activated because clock() could run any length of time before restart
		while(nextFrame < clock())
		{
			Update();
			Render();

			beginTime = clock();
			nextFrame = (nextFrame + dt * CLOCKS_PER_SEC);// - pauseTime;
			frames++;
		}

		if((nextFrame - frameTime) > CLOCKS_PER_SEC)
		{
			float x,z;
			float dirX = myPlayer->getDirection(0); float dirZ = myPlayer->getDirection(2);
			M3DVector3f frame;
			myPlayer->getPosFrame().GetForwardVector(frame);
			GLenum er = glGetError();
			x = myPlayer->getPosX();
			z = myPlayer->getPosZ();
			system("cls");
			fps = frames * CLOCKS_PER_SEC / (beginTime - frameTime);
			cout << "frameRate: " << fps << endl;
			cout << "error: " << er << endl;
			printf("Player X:%4.2f\n", x);
			printf("Player Z:%4.2f\n", z);
			printf("Test X:%4.2f\n", testObject->getPosX());
			printf("Test Z:%4.2f\n", testObject->getPosZ());
			printf("Player new Y:%4.2f\n", myPlayer->getRenderY());
			printf("Player move dir X: %4.2f\n", myPlayer->getVelX());
			printf("Player move dir Z: %4.2f\n", myPlayer->getVelZ());
			printf("Player face X: %4.2f\n", frame[0]);
			printf("Player face dir Z: %4.2f\n", frame[2]);
			cout << "is the player on the ground: "<<  myPlayer->getOnGround() << endl;
			if(myPlayer->getBounds())
				printf("Map Y:%4.2f\n", myWorld->GetScaledHeightAtPoint(x,z));
			else
				cout << "No long within the level  " << ": " << endl;
			printf("Camera Angle:%4.2f\n", myCamera->getAngle());
			//printf("Error:%GLEnum", glGetError());
			//sprintf(scrPrint, "Fps:%4.2f", fps);			
			frameTime = clock();
			frames = 0;
		}

	}//end pause check


	//some house keeping...this level holds onto main glut controls
        
    // Tell GLUT to do it again
   glutPostRedisplay();
	
	//cout << "times: " << endl;
}

void GameClass::Reshape(int w, int h)
{
	//sWorld->ChangeSize(w,h);

		glViewport(0, 0, w, h);
	
    // Create the projection matrix, and load it on the projection matrix stack
	viewFrustum.SetPerspective(65.0f, float(w)/float(h), 1.0f, 1000.0f);
	projectionMatrix.LoadMatrix(viewFrustum.GetProjectionMatrix());
   
    // Set the transformation pipeline to use the two matrix stacks 
	transformPipeline.SetMatrixStacks(modelViewMatrix, projectionMatrix);
}

void GameClass::CleanUp()
{
	glutIgnoreKeyRepeat(GLUT_KEY_REPEAT_ON);
	myWorld->~FirstTerrain();
	mySky->CleanUp();
	TextureManager* texs = TextureManager::GetInstance();
	texs->cleanUp();
	delete texs;
	delete myPlayer;
	delete testObject;
	//texs->DestroyInstance();
	//delete mySky;
}


//////////////////////////////////////////////////
//////
//
//User Interface methods
//Just passing along the values to the relavent classes
//

void GameClass::KeyDown(unsigned char key, int x, int y)
{
	myPlayer->KeyDown( key, x, y );
}

void GameClass::KeyUp(unsigned char key, int x, int y)
{
	myPlayer->KeyUp( key, x,  y );
}

void GameClass::mouseMotion(int x, int y)
{
	myCamera->mouseMotion(x,y);
}

void GameClass::mouseFunc(int button, int state,int x, int y)
{
	myCamera->mouseFunc(button,state,x,y);
}