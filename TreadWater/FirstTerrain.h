////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604
#ifndef	FIRST_TERRAIN_H
#define FIRST_TERRAIN_H

#define WIN32

#include "CTerrain.h"
#include "TerrainBatch.h"

#include <GLFrame.h>
#include <GLMatrixStack.h>
#include <GLGeometryTransform.h>

#define FREEGLUT_STATIC
#include <GL/glut.h>

#include "stafx.h"

#define MAX_NO_TERRAIN_BLOCKS 1024

#define NO_VERTS_BLOCK	8


class FirstTerrain : public CTerrain
{

protected:
	
	GLFrame			blockFrames[MAX_NO_TERRAIN_BLOCKS];
	TerrainBatch*	terrainBlocks[MAX_NO_TERRAIN_BLOCKS];
	int m_iNoBlkSide, m_iTotBlks;

private:
	TerrainBatch floorBatch;
	GLFrame floorFrame, floorFrame2;

	GLBatch triangleBatch;

	GLint	myIdentityShader;
	GLint	myLocMVP;
	GLint	myLocMV;
	GLint	myLocNM;		//normal matrix uniform
	GLuint	myTex;
	GLint	myLocColourMap;		//do i need a colour map uniform?
	GLint	myLocLight;
	//lights
	GLint	myLocAmbient;
	GLint	myLocDiffuse;

	void SetPlayerStart();
	bool generateShader();
	bool generateTerrainPatch( TerrainBatch *newPatch, int mapX, int mapZ );


public:

	int rightX, rightZ, rightY;
	GLFrame getO()		{	return floorFrame;	};

	void flattenSquare( int centreX, int centreZ, int radius, float desScaleHeight );

	float getHeightInversed(int x, int z);

	void TidyUp();	

	void Setup();
	void Render(GLShaderManager &shaderManager, GLMatrixStack &modelViewMatrix, GLGeometryTransform &transformPipeline );

	FirstTerrain( )	{  }
	~FirstTerrain( );
};

#endif