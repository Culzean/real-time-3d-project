#include "Sounds.h"

HSTREAM Sounds::loadStream(char *filename)
{	
	if (stream=BASS_StreamCreateFile(FALSE,filename,0,0,0))
		cout << "stream " << filename << " loaded!" << endl;
	else
	{
		cout << "Can't load stream";
		exit (0);
	}
	return stream;
}

Sounds::Sounds()
{
	initBass();
	stream = NULL;
	ch = NULL;
	streams = new HSTREAM[SAMPLES_NUMBER];
	streams[BACKGROUND_MUSIC] = loadStream("Data/Sounds/backgroundMusic.wav");
	BASS_ChannelSetAttribute(streams[BACKGROUND_MUSIC], BASS_ATTRIB_VOL, 0.1); // function to change the volume of sound
	streams[HORN_SOUND] = loadStream("Data/Sounds/hornSound.wav");
	BASS_ChannelSetAttribute(streams[HORN_SOUND], BASS_ATTRIB_VOL, 0.1);
	streams[BATTLE_SOUND] = loadStream("Data/Sounds/battleSound.wav");
	BASS_ChannelSetAttribute(streams[BATTLE_SOUND], BASS_ATTRIB_VOL, 0.1);
	streams[ATTACK1_SOUND] = loadStream("Data/Sounds/attack1.wav");
	BASS_ChannelSetAttribute(streams[ATTACK1_SOUND], BASS_ATTRIB_VOL, 0.1);
	streams[ATTACK2_SOUND] = loadStream("Data/Sounds/attack2.wav");
	BASS_ChannelSetAttribute(streams[ATTACK2_SOUND], BASS_ATTRIB_VOL, 0.1);
	streams[ATTACK3_SOUND] = loadStream("Data/Sounds/attack3.wav");
	BASS_ChannelSetAttribute(streams[ATTACK3_SOUND], BASS_ATTRIB_VOL, 0.1);
}

Sounds::~Sounds()
{
	for(int i=0; i<SAMPLES_NUMBER; i++)
	{
		BASS_ChannelStop(streams[i]);
	}
	delete[] streams;
}

/* Initialize default output device */
void Sounds::initBass()
{
	if (!BASS_Init(-1,44100,0,0,NULL))
		cout << "Can't initialize device" << endl;
}

void Sounds::playSound(int soundNumber)
{
	ch=BASS_SampleGetChannel(streams[soundNumber],FALSE);
	if (!BASS_ChannelPlay(streams[soundNumber], FALSE))
		cout << "Can't play sample" << endl;
	if (soundNumber == BACKGROUND_MUSIC)
		BASS_ChannelFlags(streams[soundNumber],BASS_SAMPLE_LOOP,BASS_SAMPLE_LOOP); // when background music is played, it loops
}

