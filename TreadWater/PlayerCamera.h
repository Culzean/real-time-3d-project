////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#ifndef PLAYER_CAMERA
#define PLAYER_CAMERA

#include "stafx.h"

static M3DVector3f cameraForwardV = {0.0f,0.0f,0.0f};

class PlayerCamera{
private:

	M3DMatrix44f		m_MatrixMV;

	M3DVector3f*		m_vDown;
	GLFrame				m_frCamera;
	GLFrame				m_frDir;
	GLFrame				m_frHigh;
	PlayerUI*			m_pPlayer;

	int mouseX, mouseY;
	float viewAngle;
	float rotAngle;
	float camAngle;

	//position relative to the player's pos
	//float cameraX, cameraY, cameraZ;
public:

	M3DVector3f			m_vForward;
	M3DVector3f			m_vOrigin;

	PlayerCamera(PlayerUI* playerRef);
	~PlayerCamera();

	bool init();
	void setForward();
	void update(GLFrame playerOrg, float newHeight);
	void mouseFunc(int button, int state, int x, int y);
	void mouseMotion( int x, int y );
	void keyOperations( unsigned char key, int x, int y );

	GLFrame	getFrCam()					{	return m_frDir;	};
	GLFrame getPos()					{	return m_frCamera;	};
	GLFrame getHighFrame( int iMap );
	void	setPos(GLFrame newPos)		{	m_frCamera = newPos;	};

	float	getCamAngle()				{	return camAngle;	};
	float	getAngle()					{	return viewAngle;	};
	float	getRotAngle()				{	return rotAngle;	};

};

#endif