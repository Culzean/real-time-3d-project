////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#ifndef GAMECLASS_H
	#define GAMECLASS_H

#include "stafx.h"
#include <stdio.h>
#include <stdlib.h>



class GameClass
{
private:
	static GameClass* pGame;

	bool pause;//variable switch if p is pressed
	int counter;
	float dt;
	float mouseX, mouseY;
	clock_t pauseTime;
	clock_t ticks;
	clock_t beginTime;
	clock_t nextFrame;
	clock_t frameTime;
	int		frames;

	GLShaderManager		shaderManager;			// Shader Manager
	GLMatrixStack		modelViewMatrix;		// Modelview Matrix
	GLMatrixStack		projectionMatrix;		// Projection Matrix
	GLFrustum			viewFrustum;			// View Frustum
	GLGeometryTransform	transformPipeline;		// Geometry Transform Pipeline

	GLTriangleBatch		torusBatch;
	GLBatch				floorBatch;

	/////////////////
	//key game classes
	FirstTerrain*	myWorld;
	SkyBox*			mySky;
	PlayerUI*		myPlayer;
	PlayerCamera*	myCamera;
	Objectives*		myObjectives;

	int				crtLvlSize;//you could ask the player to chose this!

	float				fps;
	bool				freeLook;

public:
	GameClass() {};

	static GameClass* GetInstance(){
	if(! pGame)
		pGame = new GameClass;
	return pGame;
	};
	static void DestroyInstance(){
		if(pGame)
		{delete pGame; pGame = NULL;}}

	void CleanUp();

	void KeyUp(unsigned char key, int x, int y);
	void KeyDown(unsigned char key, int x, int y);
	void Keyboard(unsigned char key, int x, int y);
	void KeyOperations(void);
	void mouseMotion(int x, int y);
	void mouseFunc(int button, int state,int x, int y);
	void Display(void);
	void Reshape(int w, int h);
	void SetupWindow(int argc, char* argv[]);
	bool Init(int iSize, int iHilly);
	void Update();
	void Render();
	void RenderDebug();

	void drawColBox(M3DVector3f p1, M3DVector3f p2);
};


#endif