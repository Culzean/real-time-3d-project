////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#include "CTerrain.h"


/////////////////////////////////////////////////////////////////////
//
//Key method to load height map from raw file
//using C style syntax for file reading
//////////////////////
bool CTerrain::LoadHeightMap( char* szFileName, int iSize )
{
	//check the file is correct. check if we have data loaded
	//make space. and then load

	m_iSize = iSize;

	FILE* pFile;


	//check data
	if( m_heightData.m_ucpData != NULL ) //this check is firing when it shouldn't!
		//UnloadHeightMap();
		cout << "boo";
	pFile = fopen( szFileName, "rb");	//don't know what "rb" means
		if(pFile == NULL)
		{
			//something is wrong. bad filename
			cout << "This file could not be read. Please check filename: " << szFileName << endl;
				return false;
		}

	//set out some memory for this operation
		m_heightData.m_ucpData = new unsigned char[iSize * iSize];

	//check that allocation
		if( m_heightData.m_ucpData == NULL)
		{
			cout << "Could not write this file to memory. " << szFileName << endl;
			return false;
		}

	//pass this file and the size plus pFile to fRead. what is 1?
		fread(m_heightData.m_ucpData, 1, iSize*iSize, pFile);

		//clear memory
	fclose( pFile );

	//set the size of this data file
	m_heightData.m_iSize = iSize;
	m_iSize				= m_heightData.m_iSize;
	m_bBuilt = false;

	cout << "This file has been loaded as a new height map! " << szFileName << endl;

	return true;
}

////////////////////////////////////////////////////////////////
//
//This method will create a new heightmap
//It will take a filename and create this map as a raw file
//from the data in m_heightData
bool CTerrain::SaveHeightMap( char* szFileName )
{
	FILE* pFile;

	//must open the file first
	pFile = fopen( szFileName, "wb");	//what is this arguement?

	if(pFile == NULL)
	{
		//File name is wrong?
		cout << "This file could not be read. Please check filename: " << szFileName << endl;
		return false;
	}

	if( m_heightData.m_ucpData == NULL )
	{
		cout << "There is no height data in the buffer to write. Aborted. " << szFileName << endl;
		return false;
	}

	fwrite( m_heightData.m_ucpData, 1, m_iSize * m_iSize, pFile );

	fclose(pFile);

	cout << "File Saved: " << szFileName << endl;
	return true;
}


///////////////////////////////////////////////////////////
//
//clear out the old height map to make way for a new one
//
bool CTerrain::UnloadHeightMap( void )
{
	//quick check
	if( m_heightData.m_ucpData )
	{
		delete m_heightData.m_ucpData;

		//reset counter
		m_iSize = 0;
	}

	cout << "Old height map thrown out! " << endl;
	return true;
}

void CTerrain::NormalizeHeightBuffer( float* fpHeightData )
{
	float fMax, fMin;
	float fHeight;
	int i;

	fMax = fpHeightData[0];
	fMin = fpHeightData[0];

	for(i=1; i<m_iSize * m_iSize; i++)
	{
		//go through every data point and find the max and min values
		if( fpHeightData[i] > fMax )
			fMax = fpHeightData[i];
		else if( fpHeightData[i] < fMin )
			fMin = fpHeightData[i];
	}

	if( fMax <= fMin )
		return;//what's wrong?

	fHeight = fMax - fMin;

	//and now scale the values to a range of 0-255
	//make each value a unit value of the total range then times by 255
	for(i=0; i<m_iSize * m_iSize; i++)
		fpHeightData[i] = ( ( fpHeightData[i] - fMin) / fHeight ) * 255.0f;
}

bool CTerrain::MakeTerrainFault( int iSize, int iIterations, int iMinDelta, int iMaxDelta, float fFilter)
{
	float* fTempBuffer;	//how many values does this hold?
	int iCurntIteration;
	int iHeight;
	int iRndX1, iRndZ1;
	int iRndX2, iRndZ2;
	int iDirX1, iDirZ1;
	int iDirX2, iDirZ2;
	int x, z;
	int i;
	CRandom	rndNumbGen;
	rndNumbGen.Randomize();

	//if( m_heightData.m_ucpData )	//This check has been misfiring!
		//UnloadHeightMap();

	m_iSize = iSize;

	//allocate memory
	m_heightData.m_ucpData = new unsigned char [m_iSize * m_iSize];
	fTempBuffer = new float[m_iSize * m_iSize];	//The algorithm wears the numbers down. We must keep this detail;

	if(m_heightData.m_ucpData == NULL)
	{
		cout << "Could not write the file for new height map!" << endl;
		return false;
	}

	if( fTempBuffer == NULL)
	{
		cout << "Could not allocate memory for new height map!" << endl;
		return false;
	}

	for(int i= 0; i<m_iSize*m_iSize; i++)
		fTempBuffer[i] = 0;		//reset each value in temp buffer


	for( iCurntIteration =0; iCurntIteration < iIterations; iCurntIteration++ )
	{
		//main algorithm loop. keep number of iterations to a power of 2.
		//Actually building the numbers up. With each pass we shall reduce the height range we will work the data within
		//ratio of curntIt/NoItr times the range of possible deltas subtracted through MaxDelta
		iHeight = iMaxDelta - ( ( iMaxDelta - iMinDelta) * iCurntIteration ) / iIterations;

		//two random points
		iRndX1 = rndNumbGen.Random(m_iSize);
		iRndZ1 = rndNumbGen.Random(m_iSize);

		//and again. Must be different
		do
		{
			iRndX2 = rndNumbGen.Random(m_iSize);
			iRndZ2 = rndNumbGen.Random(m_iSize);
		}while(iRndX1 == iRndX2 && iRndZ1 == iRndZ2);

		iDirX1 = iRndX2 - iRndX1;
		iDirZ1 = iRndZ2 - iRndZ1;	//turn rndnumbs into vectors

		for(z=0; z<m_iSize; z++)
		{
			for(x=0 ; x< m_iSize; x++)
			{
				//find a new vector for each point in map
				iDirX2 = x - iRndX1;
				iDirZ2 = z - iRndZ1;

				if( (iDirX2 * iDirZ1 - iDirX1 * iDirZ2) > 0 )		//This is the 2 d cross product between current vector and line vector
					fTempBuffer[( z*m_iSize) + x] += (float)iHeight;
			}
		}

		//call for the erosion filter
		FilterHeightField( fTempBuffer, fFilter );
	}

	NormalizeHeightBuffer( fTempBuffer );

	//and load this into our real buffer
	for(z=0; z<m_iSize; z++)
	{
		for(x=0 ; x< m_iSize; x++)
			SetHeightAtPoint( ( unsigned char ) fTempBuffer[( z * m_iSize) + x], x, z );
	}

	if(fTempBuffer)
	{
		delete fTempBuffer;
	}

	return true;
}

void CTerrain::FilterHeightBand( float* fpBand, int iStride, int iCount, float fFilter )
{
	float fCurrent = fpBand[0];
	int i;
	int j = iStride;

	for( i =0; i< iCount -1; i++ )
	{
		fpBand[j] = fFilter * fCurrent + ( 1 - fFilter ) * fpBand[j];

		fCurrent = fpBand[j];
		j += iStride;
	}
}

void CTerrain::FilterHeightField( float* fpHeightData, float fFilter)
{
	//pointer to a temp object fpHeightData
	int i =0;

	//erode left to right
	for( i=0 ; i< m_iSize ; i++)
		FilterHeightBand( &fpHeightData[ m_iSize * i ],	1, m_iSize, fFilter );

	//erode right to left
	for( i=0 ; i< m_iSize ; i++)
		FilterHeightBand( &fpHeightData[ m_iSize * i + m_iSize - 1], -1, m_iSize, fFilter );

	//erode top to bottom
	for( i=0 ; i< m_iSize ; i++)
		FilterHeightBand( &fpHeightData[ i ], m_iSize, m_iSize, fFilter );

	//erode bottom to top
	for( i=0 ; i< m_iSize ; i++)
		FilterHeightBand( &fpHeightData[ m_iSize * (m_iSize -1) + i ], -m_iSize, m_iSize, fFilter);
		
}