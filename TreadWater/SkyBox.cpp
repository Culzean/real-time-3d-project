////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604

#include "SkyBox.h"

const char* cubeFaces[6] = {	"res/pos_x.tga", "res/neg_x.tga", "res/pos_y.tga", "res/neg_y.tga", "res/pos_z.tga", "res/neg_z.tga"	};



void SkyBox::Init()
{
	m_enSky[0] = GL_TEXTURE_CUBE_MAP_POSITIVE_X;		m_enSky[1] = GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
	m_enSky[2] = GL_TEXTURE_CUBE_MAP_POSITIVE_Y;		m_enSky[3] = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
	m_enSky[4] = GL_TEXTURE_CUBE_MAP_POSITIVE_Z;		m_enSky[5] = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;

	LoadSky();
}

void SkyBox::LoadSky()
{
	GLbyte* pBytes;
	GLint iWidth, iHeight, iComponents;
	GLenum eFormat;

	glGenTextures(1, &cubeTexture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTexture);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);										///these arguments are set so that call to glTexImage2D is set correctly. i think

	for(int i=0; i<6; i++)
	{
		pBytes = gltReadTGABits( cubeFaces[i], &iWidth, &iHeight, &iComponents, &eFormat );
		glTexImage2D( m_enSky[i], 0, iComponents, iWidth, iHeight, 0, eFormat, GL_UNSIGNED_BYTE, pBytes);
		free(pBytes);
	}

	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

	gltMakeCube(skyBatch, 200.0f);//doesn't matter much the size..
	
	viewFrame.MoveForward(-100.0f);

	m_iSkyBoxShader = gltLoadShaderPairWithAttributes("res/SkyBox.vp", "res/SkyBox.fp", 2,
														GLT_ATTRIBUTE_VERTEX, "vVertex",
														GLT_ATTRIBUTE_NORMAL, "vNormal" );

	m_iLocMVPSky = glGetUniformLocation(m_iSkyBoxShader, "mvpMatrix");

}

SkyBox::~SkyBox()
{
	
}

void SkyBox::CleanUp()
{
	glDeleteTextures(1, &cubeTexture);
}

void SkyBox::Render(GLFrame cameraFrame, GLMatrixStack &modelViewMatrix, GLGeometryTransform &transformPipeline)
{
	M3DMatrix44f	mCameraRot;
	
	cameraFrame.GetCameraMatrix(mCameraRot, true);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_BUFFER);
	modelViewMatrix.PushMatrix();
		modelViewMatrix.MultMatrix(mCameraRot);
		glUseProgram(m_iSkyBoxShader);
		glUniformMatrix4fv(m_iLocMVPSky, 1, GL_FALSE, transformPipeline.GetModelViewProjectionMatrix());
		skyBatch.Draw();
	modelViewMatrix.PopMatrix();
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_BUFFER);
}

SkyBox::SkyBox()
{
	//want to be able to take tga file names as argument
}